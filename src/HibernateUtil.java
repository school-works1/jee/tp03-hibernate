import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * Singleton desgin pattern
 */
public class HibernateUtil {

    private static Session session;

    private HibernateUtil() {
    }

    public static Session getInstance() {
        if ( session == null )
            init();
        return session;
    }

    public static void init() {
        SessionFactory sf = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
        session = sf.openSession();
    }

    public static void destroy() {
        if ( session != null )
            session.close();
    }
}
