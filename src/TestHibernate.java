import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class TestHibernate {

    public static void main(String[] args) {
        SessionFactory sf =
                new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();

        ClientDAO dao = new ClientDAO(HibernateUtil.getInstance());

//        Client client = new Client((long) 23423, "kahlil", "abdlehamdi", 23, "Khouribga");
//        long id = dao.create(client);
//        System.out.println("User create with id: " + id + client);

//        Client client = dao.read((long)23423);
//        if ( client != null ) {
//            client.setNom("new name");
//            dao.update(client);
//            System.out.println("updated client: " + client);
//        }

        Client client = dao.read((long)23423);
        dao.delete(client.getCode());
        System.out.println("client has been deleted");

    }
}
