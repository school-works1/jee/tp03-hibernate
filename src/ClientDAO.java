import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

public class ClientDAO {

    private Session session;

    public ClientDAO(Session session) {
        this.session = session;
    }

    public Long create(Client user) {
        Transaction transaction = session.beginTransaction();
        long id = (Long) session.save(user);
        transaction.commit();
        return id;
    }

    public Client read(Long id) {
        Client user = (Client) session.get(Client.class, id);
        return user;
    }

    public List<Client> readAll() {
        return new ArrayList<>();
    }

    public void update(Client user) {
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(user);
        transaction.commit();
    }

    public Client delete(Long id) {
        Transaction transaction = session.beginTransaction();
        Client found = (Client) session.get(Client.class, id);
        if ( found != null )
            session.delete(found);
        transaction.commit();
        return found;
    }
}
